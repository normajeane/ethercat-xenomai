/*
 * =====================================================================================
 *
 *       Filename:  rftec02_r0.h
 *
 *    Description:  ROBOTUS
 *
 *        Version:  1.0
 *        Created:  2020년 12월 11일 11시 49분 56초
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */



// FOR RFTEC02 & RFT SENSOR INTERFACE
// command types
#define CMD_NONE					(0)
#define CMD_GET_PRODUCT_NAME		(1)
#define CMD_GET_SERIAL_NUMBER		(2)
#define CMD_GET_FIRMWARE_VER		(3)
#define CMD_SET_ID					(4)	// Only CAN version
#define CMD_GET_ID					(5) // Only CAN version
#define CMD_SET_COMM_BAUDRATE		(6) // Only UART version, CAN : 1Mbps Fixed
#define CMD_GET_COMM_BAUDRATE		(7) 
#define CMD_SET_FT_FILTER			(8)
#define CMD_GET_FT_FILTER			(9)
#define CMD_FT_ONCE					(10) 
#define CMD_FT_CONT					(11) 
#define CMD_FT_CONT_STOP			(12)
#define CMD_RESERVED_1				(13)
#define CMD_RESERVED_2				(14)
#define CMD_SET_CONT_OUT_FRQ 		(15)	
#define CMD_GET_CONT_OUT_FRQ  		(16)	
#define CMD_SET_BIAS				(17)
#define CMD_GET_OVERLOAD_COUNT		(18)

// OUTPUT FRQ. DEFINITIONS
#define OUTPUT_FRQ_200Hz_DEFAULT	(0)
#define OUTPUT_FRQ_10Hz				(1)
#define OUTPUT_FRQ_20Hz				(2)
#define OUTPUT_FRQ_50Hz				(3)
#define OUTPUT_FRQ_100Hz			(4)
#define OUTPUT_FRQ_200Hz			(5)
#define OUTPUT_FRQ_300Hz			(6)
#define OUTPUT_FRQ_500Hz			(7)
#define OUTPUT_FRQ_1000Hz			(8)

// FILTER SET VALUE DEFINITIONS
#define FILTER_NONE			(0) // NONE
#define FILTER_1ST_ORDER_LP	(1) // 1st order low-pass filter

// Cutoff Frq.
#define CUTOFF_1Hz		(1)
#define CUTOFF_2Hz		(2)
#define CUTOFF_3Hz		(3)
#define CUTOFF_5Hz		(4)
#define CUTOFF_10Hz		(5)
#define CUTOFF_20Hz		(6)
#define CUTOFF_30Hz		(7)
#define CUTOFF_40Hz		(8)
#define CUTOFF_50Hz		(9)
#define CUTOFF_100Hz	(10)
#define CUTOFF_150Hz	(11)
#define CUTOFF_200Hz	(12)
#define CUTOFF_300Hz	(13)
#define CUTOFF_500Hz	(14)

// PROCESSING DATA MAPPING FOR RFT EC02
// RFT data field index in RFT EC02 EtherCAT
#define IDX_D1 (0) 
#define IDX_D2 (1)
#define IDX_D3 (2)
#define IDX_D4 (3)
#define IDX_D5 (4)
#define IDX_D6 (5)
#define IDX_D7 (6)
#define IDX_D8 (7)
#define IDX_D9 (8) 
#define IDX_D10 (9)
#define IDX_D11 (10)
#define IDX_D12 (11)
#define IDX_D13 (12)
#define IDX_D14 (13)
#define IDX_D15 (14)
#define IDX_D16 (15)

#define IDX_RAW_FT1 (16) // 2 BYTES
#define IDX_RAW_FT2 (18) // 2 BYTES
#define IDX_RAW_FT3 (20) // 2 BYTES
#define IDX_RAW_FT4 (22) // 2 BYTES
#define IDX_RAW_FT5 (24) // 2 BYTES
#define IDX_RAW_FT6 (26) // 2 BYTES

#define IDX_OVERLOAD 	(28)
#define IDX_ERRLR_FLAG  (29)


void cvtBufferToInt16( short *value, unsigned char *buff );
void changeRFTEC01_ConfigParam( unsigned char param1, unsigned char param2, unsigned char param3, unsigned char param4 );
void updateRFT_Data( void );
void RFTEC02_R0_TEST();
void update_RFTEC02_R0_OutputProcessData(void);
