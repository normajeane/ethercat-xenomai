/*
 * =====================================================================================
 *
 *       Filename:  ex01.c
 *
 *    Description:  Hello world example with xenomai
 *
 *        Version:  1.0
 *        Created:  2020년 12월 02일 17시 20분 42초
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <alchemy/task.h>

RT_TASK hello_task;

// function to be executed by task
void helloWorld(void *arg)
{
    RT_TASK_INFO curtaskinfo;
    printf("hello world\n");

    // inquire current task
    rt_task_inquire(NULL, &curtaskinfo);

    // print task name
    printf("Task name: %s \n", curtaskinfo.name);

}

int main(int argc, char *argv[])
{
    char str[10];
    printf("start task\n");
    sprintf(str, "hello");

    /* Create task
     * Arguments: &task, name, stack size(0=default), priority, mode(FPU, start suspended,...) */
    rt_task_create(&hello_task, str, 0, 50, 0);
    // rt_task_spawn(&hello_task, str, 0, 50, 0, &helloWorld, 0);

    /* Start task
     * Arguments: &task, task function, function argument */
    rt_task_start(&hello_task, &helloWorld, 0);
}
