![ethercat](./figure/ethercat.jpg)
<!--![cat](./figure/cat.gif)-->

# 1. What is EtherCAT?
- EtherCAT : **Ether**net for **C**ontrol **A**utomation **T**echnology by Beckhoff. [^1]

![principle](./figure/EthercatOperatingPrinciple.svg) [^7]

## 1.1 Pre-conditions for communication[^9]

![connection](./figure/connectionEtherCAT.png)
- If multiple controllers are in use, connect each EtherCAT OUT port to the EtherCAT IN port of the next controller.
- The EtherCAT OUT port of the last controller(slave) in the chain remains free. A telegram coming from the master passes through all the slaves and is then sent back to the master using the same cable.

## 1.2 Sync Manager

- The PDOs and SDOs are read from the EtherCAT frame by the SyncManager (Reveive Parameter), or are incorporated in the EtherCAT frame (Transmit Parameter). Four Sync channels are available for data transmission:

|SyncManager channel|	Function|
|:---:|---|
|0	|Transmission of the service data **from** the EtherCAT frame **into** the mailbox(Receive SDO)|
|1	|Transmission of the service data **from** the mailbox **into** the EtherCAT frame(Transmit SDO)|
|2	|Transmission of the process data **from** the EtherCAT frame(Receive PDO 1/2/3/4)|
|3	|Transmission of the process data **into** the EtherCAT frame(Transmit PDO 1/2/3/4)|

- *Receive* or *Transmit* reference to slave, not master!
- PDO communication becomes possible in the **operational** state.
- SDO communication becomes possible in the **pre-operational** , **safe-operational**, **operational** state.

## 1.3 CANopen over EtherCAT(CoE)
- CANopen telegrams can be up to **250 bytes** long sein and thus have more capacity than the original CAN telegrams with only **8 bytes**.

- Object Dictionary

	The object dictionary contains parameters, set values and actual values of a drive. The object dictionary is the link between the application (drive functions) and the communication services. All objects in the object dictionary can be addressed by a 16bit index number(0x1000 to 0x6FFF) and an 8bit subindex(0x00 to 0xFF).

	|Index|	Assignment of the objects|
	|---|---|
	|0x1000 to 0x1FFF	|Communication profile area(CiA 301)|
	|0x2000 to 0x5FFF	|Manufacturer-specific profile area|
	|0x6000 to 0x6FFF	|Drive profile area(CiA 402)|

## 1.4 CiA 402(Drives and Motion Control)

- PDO mapping in the standard configuration [^10]

	This object is organized bit-wise. The bits have the following meaning:

    - Controlword (0x6040)
	    - Switch on: bit 0
	    - Enable voltage: bit 1
	    - Quick stop: bit 2
	    - Enable operation: bit 3
	    - Fault reset: bit 7
	    - Halt: bit 8
    - Statusword (0x6041)
	    - Ready to switch on: bit 0
	    - Switched on: bit 1
	    - Operation enabled: bit 2
	    - Fault: bit 3
	    - Voltage enable: bit 4
	    - Quick stop: bit 5
	    - Switch on disabled: bit 6
	    - Warning: bit 7
    - Mode of Operation (0x6060)
	    - Profile position mode: 1
	    - Profile velocity mode: 3
	    - Homing mode:            6
	    - Cyclic synchronous position mode (CSP):  8
	    - Cyclic synchronous velocity mode  (CSV):  9
	    - Cyclic synchronous torque mode    (CST):  10
    - Target torque (0x6071)
    - Target velocity (0x60FF)
    - Target position (0x607A)
    - Torque actual value (0x6077)
    - Velocity actual value (0x606C)
    - Position actual value (0x6064)



[^9]: https://www.faulhaber.com/
[^10]: http://www.servotronix.com/html/stepIM_Help/HTML/home.htm#!Documents/deviceprofilesegment.htm


## 1.5 Difference between EtherCAT and CANopen[^8]

|	|EtherCAT 	|CANopen|
|:---:|:---:|:---:|
|Data and Physical Hardware Layers| 	Ethernet 	|CAN bus
|Bus Speed 	|100 Mbps 	|1 Mbps (max.)
|Transfer Mode 	|Full duplex 	|Half-duplex
|Determinism (jitter between devices) 	|As low as 1 ns 	|Typically 100-200 ns
|Max. Devices 	|65,536 	|127 (0 is reserved)
|Secondary Communication Port 	|USB 	|RS232
|Master/Slave 	|Single master with one or many slaves 	|Single or multi-master, with one or many slaves
|Automatic addressing of slaves by the Master 	|Yes 	|No
|Automatic time sync of devices by the Master 	|Yes 	|No

[^7]: https://en.wikipedia.org/wiki/EtherCAT
[^8]: https://dewesoft.com/daq/what-is-ethercat-protocol

## 1.6 EtherCAT Master

There are two most famous opensource EtherCAT Master.
“**Simple open EtherCAT Master**” (SOEM)[^3] was developed by Open EtherCAT Society(RT-Lab) and “**EtherCAT Master**” from EtherLab software develops by the Engineering Association IgH[^2].

### a) Etherlab
![](./figure/etherlab-igh.png)
These natively supported drivers and their corresponding hardware are mentioned below:

    - 8139too - RealTek 8139C (or compatible) Fast-Ethernet chipsets.
    - e1000 - Intel PRO/1000 Gigabit-Ethernet chipsets (PCI).
    - e100 - Intel PRO/100 Fast-Ethernet chipsets.
    - r8169 - RealTek 8169/8168/8101 Gigabit-Ethernet chipsets.
    - e1000e - Intel PRO/1000 Gigabit-Ethernet chipsets (PCI Express).

![etherlab](./figure/etherlab.png)

- Installation[^11]

	**This installation absolutely depends on the hardware setup! Especially, LAN Card!**

	- Installing EtherLab 2.1.0

	- Installing the IgH EtherCAT master 1.5.2[^12]

		- Clone 1.5 Stable(not 1.5.2)	

			`hg clone http://hg.code.sf.net/p/etherlabmaster/code ethercat-hg`	

			`hg update stable-1.5`

		- Configuration
			- Although you had e1000e driver, Do not use `--enable-e1000e`. (So, it will be installed with generic driver.)
			- But, if you have r8169 driver, you can use `--enable-r8169`.

		```
		cd ./ethercat-1.5.2/
		./configure --prefix=/usr/local/etherlab  --disable-8139too
		make
		make modules
		make doc
		sudo make install
		sudo make modules_install
		sudo /sbin/depmod
		```
		

[^11]: https://www.symbitron.eu/wiki/index.php?title=EtherLab_Installation
[^12]: https://www.etherlab.org/en/ethercat/index.php


### b) SOEM[^5]
SOEM is an EtherCAT master library written in c. Its purpose is to learn and to use. All users are invited to study the source to get an understanding how an EtherCAT master functions and how it interacts with EtherCAT slaves.

As all applications are different SOEM tries to not impose any design architecture. Under Linux it can be used in generic user mode, PREEMPT_RT or Xenomai. Under Windows it can be used as user mode program.

- Preconditions Linux:
    - Linux 2.6 kernel or later.
    - GCC compiler (others might work, just not tested).
    - One (or two if in redundant mode) 100Mb/s NIC that can connect to a RAW socket.
    - Application must run as root / kernel.

- Preconditions Windows:
    - Windows2000 - Windows 7 (8 not tested, might work).
    - VC compiler (others might work, just not tested).
    - One (or two if in redundant mode) 100Mb/s NIC that can connect to a RAW socket.
    - WinPcap installed.

- EtherCAT Master using SOEM
    - Freescale i.MX53
    - Blackfin 5xx
    - Blackfin 6xx
    - Intel
    - Infineon XMC47/XMC48

- EtherCAT Slave using SOES
    - Freescale K10 - using Beckhoff ET1100
    - Freescale K60 - using Beckhoff ET1100
    - Xilinx Zynq - using Beckhoff ET1815
    - Infineon XMC43/XMC48
    - Microchip LAN9252

- Installation
	- Linux & macOS
	```
	git clone https://github.com/OpenEtherCATsociety/SOEM.git
	cd SOEM
	mkdir build
	cd build
	cmake ..
	make
	```


### c) Comparison[^4]

|  | IgH Master | SOEM |
|:---:|:---:|:---:|
|Developers 	|EtherLab 	|Rt-Labs
|Licence (master) 	|GPL v2 	|GPL v2
|Licence (API) 	|LGPL v2.1 	 
|Sources 	|SourceForge (mercurial) 	|Githib (git)
|Activity (updates, …) 	|recently 	|recently
|Language 	|C, makefile, kbuild 	|C, Cmake
|System compatibility 	|Linux 2.6 + 	|Linux 2.6 +
|RT compatibility 	|all RT Linux 	|generic RT Linux
|NIC compatibility 	|Native / generic driver 	|generic driver
|Class Master 	|type A 	|type B (simple)
|Master mode 	|Multi master 	|Simple master (expendable to multi)
|Master redundant mode 	|yes 	|yes
|Distributed Clock (DC) 	|yes 	|yes
|Supported mode: 	  	 
|CoE (SDO/PDO) 	|yes 	|yes
|SoE(Servo-drive over EtherCAT) 	|yes 	|yes
|EoE(Ethernet over EtherCAT) 	|yes 	|not yet
|FoE(File Access over EtherCAT) 	|yes 	|yes
|VoE(Vendor Specific over EtherCAT) 	|yes 	|not yet
|Architecture 	|Kernel module 	|free
|Platform support	| Linux with or without realtime extension(Xenomai)	| Linux, Window, MAC OS, bare-metal MCU

[^1]: https://www.ethercat.org/
[^2]: https://www.etherlab.org/
[^3]: http://openethercatsociety.github.io/
[^4]: http://ethercatcpp.lirmm.net/ethercatcpp-framework/
[^5]: https://github.com/OpenEtherCATsociety/SOEM.git/



# 2. Xenomai
![xenomai](./figure/xenomai.jpg)

## 2.1 Available compatibility
| | Linux kernel(ipipe) | Xenomai | Ubuntu
|:---:|:---:|:---:|:---:|
|1|	3.8.13|2.6.3|12.04.3 LTS x86
|2|	3.10.32|2.6.3|12.04.3 LTS x86
|3|	3.18.20|2.6.5|14.04/16.04
|4|	4.1.18|3.0.3|14.04
|5|	**4.9.38**|**3.0.5**|**16.04**
|6|	4.14.85|3.0.8|18.04

5: Ubuntu 18.04에서 'CLOCK_MONOTONIC' 관련 오류 발생


## 2.2 Installation [^6]

1. Get the linux kernel
```
wget https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.9.38.tar.gz
tar xf linux-4.9.38.tar.gz
```

2. Get Xenomai 3.0.5
```
wget https://xenomai.org/downloads/xenomai/stable/xenomai-3.0.5.tar.bz2
tar xf xenomai-3.0.5.tar.bz2
```

3. Apply the Xenomai patch
```
cd linux-4.9.38
wget https://xenomai.org/downloads/ipipe/v4.x/x86/ipipe-core-4.9.38-x86-4.patch
../xenomai-3.0.5/scripts/prepare-kernel.sh --arch=x86_64 --ipipe=ipipe-core-4.9.38-x86-4.patch
```
 - Small fix

    - ipipe-core-4.9.38-x38-**3**.patch :arrow_right: ipipe-core-4.9.38-x38-**4**.patch

4. Configure the kernel

5. Build the Real-time kernel

	`sudo apt install kernel-package`

  - Small fix
(error: openssl/opensslv.h: No such file or directory)

    - **`sudo apt install libssl-dev`** 
6. Allow non-root users

7. Configure GRUB and reboot

8. Test your installation

	`cd /usr/xenomai/bin`

	`sudo ./xeno latency`





# 3. Experiment

## 1.1 Xenomai + SOEM
![experiment](./figure/experiment.svg)


### a. Hardware
- Intel NUC i3

- Motor(Maxon motor)

	- Motor: DCX32L GB KL 24V

	- Gear: GPX32HP 21:1

	- Encoder: ENC 30 AEDL 5810 1024IMP

	- Controller: MAXPOS 50/5


- Torque sensor(ROBOTUS)

	- RFT80-6A01

	![torque_sensor](./figure/rft80.png)

	- RFTEC-02

	![torque_board](./figure/rftec-02.png)





[^6]: https://rtt-lwr.readthedocs.io/en/latest/rtpc/xenomai3.html 
